//
//  SearchViewController.swift
//  AssignmentGetGoing
//
//  Created by mcda on 2018-06-13.
//  Copyright © 2018 Ranjit IOS DEV. All rights reserved.
//

import UIKit
import CoreLocation

class SearchViewController: UIViewController {
    var searchParam: String?
    
    var rankBy = "Prominence"
    var distance = "25000"
    var openNow = false
    var location: CLLocation?
    
    @IBOutlet weak var selectSegmentIndex: UISegmentedControl!
    
    // MARK: View Load
    override func viewDidLoad() {
        super.viewDidLoad()
        searchParameterTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: searchParameter Text Field
    @IBOutlet weak var searchParameterTextField: UITextField!
    
    //MARK: Search Button Action
    @IBAction func searchButtonAction(_ sender: UIButton) {
        print("choo choo choo button")
        searchParameterTextField.resignFirstResponder()
        
        
        if selectSegmentIndex.selectedSegmentIndex == 0 {
            if let inputValue = searchParam {
                print("success")
                GooglePlacesAPI.textSearch(query: inputValue, rank: rankBy, distance: distance, openNow: openNow, completionHandler: {(status, json) in
                    if let jsonObj = json {
                        let places = APIParser.parserAPIResponse(json: jsonObj)
                        
                        // update UI on the main thread!
                        DispatchQueue.main.async {
                            if places.count > 0 {
                                self.presentSearchResults(places)
                            } else {
                                self.generalAlert("Oops", "No results found")
                            }
                        }
                        print("\(places.count)")
                    } else {
                        self.generalAlert("Oops", "An errorparsing json")
                    }
                })
            } else {
                //          alertError()
                generalAlert("Opps!", "An error has occurred")
            }
        }
        else {
            if let latitude = location?.coordinate.latitude , let longitude = location?.coordinate.longitude {
                GooglePlacesAPI.locationSearch(lat: latitude, lng: longitude, rank: rankBy, distance: distance, openNow: openNow, completionHandler: {(status, json) in
                    if let jsonObj = json {
                        let places = APIParser.parserAPIResponse(json: jsonObj)
                        // update UI on the main thread!
                        DispatchQueue.main.async {
                            if places.count > 0 {
                                self.presentSearchResults(places)
                            } else {
                                self.generalAlert("Oops", "No results found")
                            }
                        }
                        print("\(places.count)")
                    } else {
                        self.generalAlert("Oops", "An errorparsing json")
                    }
                })
            }
        }
        //    generalAlert("Success!", "We got \(inputValue)")
        
    }
    
    func presentSearchResults(_ results: [PlaceOfInterest]) {
        let searchResultsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
        searchResultsViewController.places = results
        
        navigationController?.pushViewController(searchResultsViewController, animated: true)
    }
    
    @IBAction func searchSelectionChanged(_ sender: UISegmentedControl) {
        print("segment control changed \(selectSegmentIndex.selectedSegmentIndex)")
        if selectSegmentIndex.selectedSegmentIndex == 1 {
            LocationService.sharedInstance.delegate = self
            LocationService.sharedInstance.startUpdatingLocation()
        }
    }
    
    func generalAlert(_ title: String,_ message: String?) {
        let alertController = UIAlertController(title:title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default) {(action)
            in
            self.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(okAction)
        
        present(alertController, animated: true) {
            self.searchParameterTextField.placeholder = "Input Something"
        }
    }
    
    @IBAction func filter_Action(_ sender: Any) {
        let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        filterViewController.delegate = self
        self.present(filterViewController, animated: true, completion: nil)
    }
    

    
}
// MARK: Text Field Delegate
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchParameterTextField {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == searchParameterTextField {
            searchParam = textField.text
            print("you typed", textField.text ?? "no input")
        }
        return true
    }
    
}

extension SearchViewController: LocationServiceDelegate, FilterViewControllerDelegate {
    
    func tracingLocation(_ currentLocation: CLLocation) {
        location = currentLocation
        print("\(currentLocation.coordinate.latitude) \(currentLocation.coordinate.longitude)")
    }
    
    func tracingLocationDidFailWithError(_ error: Error) {
        
    }
    func getUpdatedQueryItems(rankBy: String, distance: String, openNow: Bool) {
        self.rankBy = rankBy
        self.distance = distance
        self.openNow = openNow
    }
}
