//
//  FilterViewController.swift
//  AssignmentGetGoing
//
//  Created by mcda on 2018-06-25.
//  Copyright © 2018 Ranjit IOS DEV. All rights reserved.
//
import UIKit


protocol FilterViewControllerDelegate {
    func getUpdatedQueryItems(rankBy: String,distance: String, openNow: Bool)
}

class FilterViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var rankBy = "Prominence"
    var distance = "25000"
    
    let rankValues = ["Prominence", "Distance"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rankPickerView.dataSource = self
        rankPickerView.delegate = self
    }
    var delegate: FilterViewControllerDelegate?
    
    @IBAction func applyAction(_ sender: Any) {
        delegate?.getUpdatedQueryItems(rankBy: rankBy, distance: distance, openNow: openSwitch.isOn)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var openSwitch: UISwitch!
    
    @IBOutlet weak var distanceSlider: UISlider!
    @IBOutlet weak var rankPickerView: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return rankValues.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return rankValues[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        rankBy = rankValues[row]
    }
    
    @IBAction func ranksliderChanged(_ sender: UISlider) {
       
        distance = "\(Int(ceil(sender.value * 50000)))"
    }
}
