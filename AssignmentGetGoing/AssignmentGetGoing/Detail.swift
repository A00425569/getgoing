//
//  Detail.swift
//  AssignmentGetGoing
//
//  Created by mcda on 2018-06-25.
//  Copyright © 2018 Ranjit IOS DEV. All rights reserved.
//

import UIKit

class Detail: NSObject {
    var phone: String?
    var website: String?
    
    init?(json: [String: Any]){
        
        self.phone = json["formatted_phone_number"] as? String
        self.website = json["website"] as? String
        
        
    }
}
