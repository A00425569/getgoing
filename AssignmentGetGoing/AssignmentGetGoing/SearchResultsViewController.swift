//
//  SearchResultsViewController.swift
//  AssignmentGetGoing
//
//  Created by mcda on 2018-06-18.
//  Copyright © 2018 Ranjit IOS DEV. All rights reserved.
//

import UIKit

class SearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var places: [PlaceOfInterest]!
    override func viewDidLoad() {
        super.viewDidLoad()

        let tableViewCellNib = UINib(nibName: "POITableViewCell", bundle: nil)
        tableView.register(tableViewCellNib, forCellReuseIdentifier: "resuableIdentifier")
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        //places.sorted { (places1, places2) -> Bool in
          //  return places1.formattedAddress! < places2.formattedAddress!
       // }
    }
    // MARK: - TableView Deligate methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    @IBAction func SgControl(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            places = places.sorted {
                $0.name < $1.name
            }
            tableView.reloadData()
            break;
        default:
            places = places.sorted {
                $0.rating! < $1.rating!
            }
            tableView.reloadData()
            break;
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resuableIdentifier") as!
        POITableViewCell
        
        cell.titleLabel.text = places[indexPath.row].name
        cell.addressLabel.text = places[indexPath.row].formattedAddress
        cell.ratingLabel.text = "\(places[indexPath.row].rating ?? 0)"
        print(places[indexPath.row].name)
        if let imageUrl = places[indexPath.row].iconUrl, let url = URL(string: imageUrl), let dataContents = try? Data(contentsOf: url), let imageSrc = UIImage(data: dataContents) {
            cell.iconImageView.image = imageSrc
            
        }
        return cell
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let detailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        detailView.place = places[indexPath.row]
        navigationController?.pushViewController(detailView, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
