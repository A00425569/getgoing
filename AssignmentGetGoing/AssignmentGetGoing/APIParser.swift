//
//  APIParser.swift
//  AssignmentGetGoing
//
//  Created by mcda on 2018-06-18.
//  Copyright © 2018 Ranjit IOS DEV. All rights reserved.
//

import Foundation

class APIParser {
    
    class func parseDetailsValue(json: [String: Any]) -> Detail?{
        
        if let result = json["result"] as? [String:Any] {
            
            if let value = Detail(json: result){
                return value
            }
            
        }
        return nil
    }
    
    class func parserAPIResponse(json: [String: Any]) -> [PlaceOfInterest] {
        var places: [PlaceOfInterest] = []
        if let results = json["results"] as? [[String: Any]] {
            for result in results {
                if let newPlace = PlaceOfInterest(json: result) {
                    places.append(newPlace)
                }
            }
        }
        return places
    }
}
